package edu.uoc.android.restservice.ui.enter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.adapter.GitHubAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoUserActivity extends AppCompatActivity {

    //ArrayList<Materia> listaFollowers;
    RecyclerView recyclerViewFollowers;

    TextView textViewNombre, textViewApellido, textViewNivel, textViewMateriasAprobadas, getTextViewMateriasReprobadas;
    ImageView imageViewProfile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_user);

        textViewNombre = findViewById(R.id.textViewNombre);
        textViewApellido = findViewById(R.id.textViewApellido);
        textViewNivel = findViewById(R.id.textViewNivel);
        textViewMateriasAprobadas = findViewById(R.id.textViewMateriasAprobadas);
        getTextViewMateriasReprobadas = findViewById(R.id.textViewMateriasReprobadas);

        imageViewProfile = (ImageView) findViewById(R.id.imageViewProfile);

        //listaFollowers = new ArrayList<>();
        recyclerViewFollowers = (RecyclerView)findViewById(R.id.recyclerViewFollowers);
        recyclerViewFollowers.setLayoutManager(new LinearLayoutManager(this));

        String cedula = getIntent().getStringExtra("cedula");
        //mostrarDatosEstudiante(cedula);
        //mostrarMaterias(cedula);
    }


}
