package edu.uoc.android.restservice.ui.enter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.adapter.GitHubAdapter;

public class EnterUserActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextCedula;
    private Button buttonConsultar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_user);
        initViews();
    }

    private void initViews() {
        editTextCedula = findViewById(R.id.enter_user_edit_text);
        buttonConsultar = findViewById(R.id.enter_user_button);
        buttonConsultar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == buttonConsultar) {
            Intent intent = new Intent(EnterUserActivity.this, InfoUserActivity.class);
            intent.putExtra("cedula", editTextCedula.getText().toString());
            startActivity(intent);
        }
    }
}
