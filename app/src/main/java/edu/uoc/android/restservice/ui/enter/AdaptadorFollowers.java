package edu.uoc.android.restservice.ui.enter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import edu.uoc.android.restservice.R;

/**
 * Created by edgardopanchana on 4/29/18.
 */

public class AdaptadorFollowers extends RecyclerView.Adapter<AdaptadorFollowers.ViewHolderFollowers> {

    //ArrayList<Materia> listaFollowers;

//    public AdaptadorFollowers(ArrayList<Materia> listaFollowers) {
//        this.listaFollowers = listaFollowers;
//    }

    @NonNull
    @Override
    public ViewHolderFollowers onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);
        return new ViewHolderFollowers(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderFollowers holder, int position) {
//        holder.textViewDescripcion.setText(listaFollowers.get(position).getDescripcion());
//        holder.textViewParcialUno.setText(listaFollowers.get(position).getParcial_uno());
//        holder.textViewParcialDos.setText(listaFollowers.get(position).getParcial_dos());
//        holder.textViewAprueba.setText(listaFollowers.get(position).getAprueba());

    }

    @Override
    public int getItemCount() {
        //return listaFollowers.size();
        //Borrar return 1;
        return 1;
    }

    public class ViewHolderFollowers extends RecyclerView.ViewHolder {

        TextView textViewDescripcion, textViewParcialUno, textViewParcialDos, textViewAprueba;


        public ViewHolderFollowers(View itemView) {
            super(itemView);

            textViewDescripcion = (TextView) itemView.findViewById(R.id.textViewDescripcion);
            textViewParcialUno = (TextView) itemView.findViewById(R.id.textViewParcialUno);
            textViewParcialDos = (TextView) itemView.findViewById(R.id.textViewParcialDos);
            textViewAprueba = (TextView) itemView.findViewById(R.id.textViewAprueba);
        }
    }
}
